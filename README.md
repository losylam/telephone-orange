Téléphone Orange
======

Programmation python/raspberry pi pour AVEAM.

Par Laurent Malys (lo@crans.org)

Version 0.9 (29/12/2018)

## Installation

### Raspian

Les raspberry pi utilisent le système d'exploitation ``raspian desktop`` version "2018-11-13".

### Dépendances

Les scripts sont développés en ``python3``.
L'interface logicielle avec les broches GPIO se fait via la librairie python ``RPi.GPIO`` installée avec ``pip``.
Les sons sont joués par ``vlc`` via son api python installée avec ``pip``.

```bash
sudo apt-get install python3 vlc python3-pip
sudo pip3 install -r requirements.txt
```

### Installation de Téléphone Orange

Le dossier ``telephone-orange`` contenant les scripts python de chaque manipulation et le script principal ``play.py`` doit être situé dans le répertoire utilisateur (home) de l'utilisateur pi : ``/home/pi``

### Démarrage automatique

Le démarrage automatique se fait en ajoutant les deux lignes suivantes au fichier ``/etc/rc.local``, avant la ligne ``exit 0`` :

```bash
cd /home/pi/telephone-orange
python3 play.py&
```

## Fonctionnement général

### Logiciel

Le script principal est ``play.py`` qui est démarré automatiquement au démarrage des raspberry pi (à l'aide de deux commandes dans le fichiers ``/etc/rc.local``).

Le choix du programme à effectuer se fait dans le fichier ``play.py`` via le paramètre ``manip``.

Le dossier ``telephone-orange`` contient quatre scripts pour cinq installations :
- ``telephone_orange.py`` : téléphone à touches orange
- ``strowger.py`` : téléphone à cadran relié au strowger
- ``standard.py`` : standard et table avec les téléphones portables
- ``téléphone_mural.py`` : téléphone mural en bois

### Électronique

Tous les interrupteurs ou contacts secs sont reliés à une broche GPIO du raspberry pi d'un côté et à la masse du raspberry de l'autre. Ces broches sont configurées de façon logicielle avec une résistance de tirage interne (pull_up).

## Téléphone orange

### Fonctionnement

Simulation d'un téléphone :
- tonalité quand on décroche le combiné (sinusoïde 440Hz)
- tonalité des touches (DTMF)
- joue un son lorsqu'un numéro de l'annuaire est tapé sur le clavier
- joue un message en boucle si le numéro n'est pas attribué

### Paramétrage

Les numéros ç effectuer correspondent aux noms des fichiers sonores correspondant, qui sont placés dans le dossier ``telephone_orange_numeros``.

Le script parcourt tous les fichiers de ce dossier et ajoute tous les fichiers avec des extensions ``.mp3``, ``.wav`` et ``.mp4`` à l'annuaire. Par exemple, pour ajouter le numéro 245854 il suffit de nommer le fichier son correspondant ``245854.mp3`` (s'il s'agit d'un mp3) dans le dossier ``telephone_orange_numeros``.

Les autres paramètres sont écrits en python dans le ``telephone_orange.py`` :
- **enable_pin**: numéro de la broche GPIO du raspberry pi à laquelle est connecté le bouton sur lequel est posé le combiné 
- **buttons_pins**: numéros des broches GPIO du raspberry pi sur lesquelles sont connectés chacun des boutons du téléphone, dans l'ordre de 0 à 9
- **longueur_max_num_orange**: longueur maximale des numéros de téléphone, à partir de laquelle le son ``non-attribue.mp3`` est joué en boucle si le numéro n'est pas dans l'annuaire (les numéros peuvent néanmoins être plus court que cette valeur)
- **sounds_number_dir**: dossier dans lequel sont placés les fichiers sons de l'annuaire
- **sounds_dir**: dossier dans lequel sont placés les autres fichiers sons :
    - **tonalite.mp3**: tonalité de départ
    - **non_attribue.mp3**: fichier joué en boucle lorsqu'un numéro n'est pas attribué
    - de **0.mp3** à **9.mp3**: tonalités DTMF des touches du téléphone


## Strowger

### Fonctionnement

Le fonctionnement est similaire à celui du téléphone orange à part en ce qui concerne la manière dont les numéros sont composé (cadran rotatifs à la place de bouton) et n'utilise ni tonalité quand le téléphone est décroché, ni tonalité des touches.

### Paramétrage

L'annuaire se compose comme pour le téléphone orange en plaçant des fichiers sons dont le nom correspond au numéro dans le dossier ``strowger_numéros``.

Le fichier lu en cas le numéro non attribué est le fichier ``non_attribue.mp3`` placé dans le dossier ``strowger_sons``

Les autres paramètres :
- **pin_enable**: broche à laquelle est connecté l'interrupteur sur lequel repose le combiné
- **pin_move**: broche à laquelle est connecté l'interrupteur qui permet de savoir si le cadran est en mouvement
- **pin_pulse**: broche sur lequel est connecté l'interrupteur qui permet de compter les chiffres pendant que le cadran tourne
- **longueur_max_num**: longueur maximale des numéros (à partir de laquelle le son ``non_attribue`` est joué sur le numéro n'est pas dans l'annuaire.

## Standard

### Fonctionnement

Joue un son lorsque l'un des pin GPIO considéré est relié à la terre.

Fonctionne pour le standard et les téléphones portables.

### Paramétrage

Les fichiers son sont placés dans le dossier ``standard``.

Deux listes définies dans le fichier python permettent de paramétrer quel son est joué suivant le numéro de la broche GPIO qui est activée :
- **pins**: numéros des broches GPIO dont l'ordre correspond à l'ordre des fichiers sons dans la liste suivante.
- **sounds_standard**: noms des fichiers sons dans l'ordre correspondant à l'ordre des broches dans la liste précédente.

## Téléphone mural

### Fonctionnement

Joue le son ``00_Sacha_Guitry.mp3`` dans le dossier ``telephone_mural``, dès que le combiné est décroché.

### Paramétrage

- **enable_pin**: broche GPIO à laquelle est connectée l'interrupteur sur lequel repose le combiné.
- **joue_si_etat**: suivant l'état de l'interrupteur sur lequel repose le combiné. 1 si l'interrupteur doit jouer le son lorsque l'interrupteur est ouvert, c'est à dire que l'interrupteur est fermé lorsque le combiné est posé, 0 sinon.
