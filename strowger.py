#!/usr/bin/env python
# coding: utf-8

"""
Standard

joue un son si un des cables est branché au standard

@author: Laurent Malys (lo@crans.org)
@license: GPL3.0
"""

import time
import os

import RPi.GPIO as GPIO
import vlc

GPIO.setmode(GPIO.BCM)

pin_enable = 16
pin_move = 20
pin_pulse = 21

longueur_max_num = 5

class Button(object):
    """
    Objet utilisé pour lire l'entrée des pins et détecter les fronts montants
    """
    def __init__(self, number=None, pin=None, filename=None):
        self.pin = pin
        self.number = number
        self.filename = filename

        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        self.state = True
        self.old_state = True

        self.player = None
        if filename:
            self.player = vlc.MediaPlayer(filename)
        
    def update(self):
        """
        renvoie le numéro en cas de front montant
        """
        self.state = GPIO.input(self.pin)

        out = None
        
        if self.state != self.old_state:
            # if self.state == False:
            #     print('on %s'%self.number)
#            else:
#                print('off %s'%self.number)
            self.old_state = self.state
            return self.state
        else:
            return None

def strowger():
    playing = None
    enable = Button(number="enable", pin=pin_enable)
    move = Button(number="move",pin=pin_move)
    pulse = Button(number="pulse", pin=pin_pulse)

    seq = ''
    cpt = 0

    # nom du répertoire contenant les sons
    sounds_dir = 'strowger_numeros'

    # initialisation liste des numéros de téléphone
    phone_numbers = []

    # initialisation liste des fichiers sons associés
    sounds = []

    # chargement et mise en boucle du fichier 'non_attribue.mp3'
    non_attribue_media = vlc.Media('strowger_sons/non_attribue.mp3')
    non_attribue_media.add_options('input-repeat=999')
    non_attribue = non_attribue_media.player_new_from_media()
    non_attribue.play()
    non_attribue.stop()

    # ajout des fichiers contenus dans le dossier 'sounds_dir' 
    for filename in os.listdir(sounds_dir):
        if filename[-4:] in ['.mp3', '.mp4', '.wav']:
            phone_numbers.append(filename[:-4])
            sounds.append(vlc.MediaPlayer(os.path.join(sounds_dir, filename)))

    for s in sounds:
        s.play()
        s.stop()

    while True:
        enable.update()
        if enable.state == 0:
            is_moving = move.update()
            if is_moving == 1:
                if (cpt > 0):
                    seq += str(cpt%10)
                    print(seq)
            if move.state == False:
                if pulse.update() == True:
                    cpt += 1
            else:
                cpt = 0

            if seq in phone_numbers and playing == None:
                playing = phone_numbers.index(seq)
                sounds[playing].stop()
                sounds[playing].play()
                seq = "0" * (longueur_max_num+1)

            elif len(seq) == longueur_max_num:
                non_attribue.play()

            time.sleep(0.01)

        else:
            seq = ''
            non_attribue.stop()
            for s in sounds:
                s.stop()
                playing = None

if __name__=="__main__":
    strowger()
