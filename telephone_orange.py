#!/usr/bin/env python
# coding: utf-8

"""
Téléphone orange

Detecte des numéros entrés dans un téléphone à cadran matriciel 
de couleur orange

joue un son si un certain numéro est entré

@author: Laurent Malys (lo@crans.org)
@license: GPL3.0
"""

import time
import os
import random

import RPi.GPIO as GPIO
import vlc

GPIO.setmode(GPIO.BCM)

# pin sur lequel est branché l'interrupteur du combiné
enable_pin_orange = 18

# pin sur lesquelles chaque touche du cadran est branchée, de 0 à 9
buttons_pins = [21, 4, 17, 5, 6, 13, 19, 27, 22, 20]

# nombre maximum de chiffres des numéros
longueur_max_num_orange = 6

# nom des répertoire contenant les sons
sounds_number_dir = 'telephone_orange_numeros'
sounds_dir = 'telephone_orange_sons'

# initialisation liste des numéros de téléphone
phone_numbers = []

# initialisation liste des fichiers sons associés
sounds = []

# sounds random
sounds_random_dir = "telephone_orange_random"
sounds_random = []

# ajout des fichiers contenus dans le dossier 'sounds_number_dir' 
for filename in os.listdir(sounds_number_dir):
    if filename[-4:] in ['.mp3', '.mp4', '.wav']:
        phone_numbers.append(filename[:-4])
        sounds.append(os.path.join(sounds_number_dir, filename))

class OrangeButton(object):
    """
    Objet utilisé pour lire l'entrée des pins et détecter les fronts montants
    """
    def __init__(self, number = None, pin = None):
        self.pin = pin
        self.number = number

        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        self.state = False
        self.old_state = False
        
    def update(self):
        """
        renvoie le numéro en cas de front montant
        """
        self.state = GPIO.input(self.pin)

        out = None
        
        if self.state != self.old_state:
            if self.state == False:
                out = self.number
            self.old_state = self.state

        return out
    
def telephone_orange():
    # chargement des sons liés aux numéros
    sound_files = []
    for sound_file in sounds:
        sound_files.append(vlc.MediaPlayer(sound_file))

    # chargement de la tonalité
    tone_path = os.path.join(sounds_dir, 'tonalite.mp3')
    tone = vlc.MediaPlayer(tone_path)
    
    # chargement des sons "random" (en cas de numéro non attribué)
    for s in os.listdir(sounds_random_dir):
        sound_random_path = os.path.join(sounds_random_dir, s)
        sound_random_media = vlc.Media(sound_random_path)
        sound_random_media.add_options('input-repeat=999')
        sound_random_media_player = sound_random_media.player_new_from_media()
        sounds_random.append(sound_random_media_player)

    # chargement des tonalités des touches
    dial_tones = []
    for i in range(10):
        dial_tone_path = os.path.join(sounds_dir, '%s.mp3'%i)
        dial_tones.append(vlc.MediaPlayer(dial_tone_path))

    # initialisation des boutons
    GPIO.setup(enable_pin_orange, GPIO.IN, pull_up_down=GPIO.PUD_UP)
    buttons = []
    for i_pin, pin in enumerate(buttons_pins):
        buttons.append(OrangeButton(number = i_pin, pin = pin))

    def stop_dial():
        """
        arrête tous les sons des touches
        """
        for i in range(10):
            dial_tones[i].stop()
        
    seq = ''
    old_state = 0

    playing = None
    
    while True:
        if GPIO.input(enable_pin_orange) == 1: # si le téléphone est décroché
            
            if old_state == 0: # détecte si le telephone vient d'être décroché
                print("decroché")
                print("was raccroché")
                tone.stop()
                tone.play()

            for b in buttons:
                pressed = b.update()
                if pressed != None: # si un bouton vient d'être appuyé
                    tone.stop()
                    stop_dial()
                    dial_tones[pressed].play()
                    seq = seq + str(pressed)
                    print(seq)
                time.sleep(0.001)
                
            if seq in phone_numbers: # si un numéro a été reconnu
                print('%s appelé!'%seq)
                i_number = phone_numbers.index(seq)
                seq = '0'*(longueur_max_num_orange+1)
                sound_files[i_number].stop()
                sound_files[i_number].play()
                playing = i_number
            
            elif len(seq) == longueur_max_num_orange: # sinon et que le numéro fait 6 chiffres ou plus
                #non_attribue.play()
                seq = '0'*(longueur_max_num_orange+1)
                choisi = random.randint(0, len(sounds_random)-1)
                print("non attribue: %s"%choisi)
                sounds_random[choisi].play()
                
            old_state = 1
            
        else:
            # si le téléphone est raccroché, on arête tous les sons
            # et on réinitialise le numéro
            #non_attribue.stop()
          
            for s in sounds_random:
                s.stop();
            tone.stop()
            stop_dial()
            if playing != None:
                sound_files[playing].stop()
                playing = None
                
            seq = ''
            if old_state == 1:
                print("raccroché")
                print("was decroché")
            old_state = 0
            
            time.sleep(0.1)

if __name__=="__main__":
    telephone_orange()
