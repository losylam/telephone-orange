#!/usr/bin/env python
# coding: utf-8

"""
Standard

joue un son si un des cables est branché au standard

@author: Laurent Malys (lo@crans.org)
@license: GPL3.0
"""

import time
import os

import RPi.GPIO as GPIO
import vlc

GPIO.setmode(GPIO.BCM)

pins = [5, 6, 13, 17, 27, 22, 23, 24, 25, 4]
sounds_standard = ["07.mp3", "13.mp3", "21.mp3", "29.mp3", "38.mp3", "44.mp3", "46.mp3", "70.mp3", "72.mp3", "87.mp3"]

class StandardButton(object):
    """
    Objet utilisé pour lire l'entrée des pins et détecter les fronts montants
    """
    def __init__(self, number=None, pin=None, filename=None):
        self.pin = pin
        self.number = number
        self.filename = filename

        GPIO.setup(self.pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        self.state = False
        self.old_state = False

        self.player = vlc.MediaPlayer(filename)
        
    def update(self):
        """
        renvoie le numéro en cas de front montant
        """
        self.state = GPIO.input(self.pin)

        out = None
        
        if self.state != self.old_state:
            if self.state == False:
                print('play %s'%self.number)
                self.player.play()
            else:
                self.player.stop()
                print('stop %s'%self.number)
            self.old_state = self.state

def standard():
    buttons = []
    for i_pin, pin in enumerate(pins):
        filename = os.path.join('standard', sounds_standard[i_pin])
        buttons.append(StandardButton(number = i_pin, pin = pin, filename=filename))

    while True:
        for b in buttons:
            b.update()
            time.sleep(0.01)

                
if __name__ == "__main__":
    standard()
