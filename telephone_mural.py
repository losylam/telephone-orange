#!/usr/bin/env python
# coding: utf-8

"""
Téléphone mural

joue un son si le combiné est décroché

@author: Laurent Malys (lo@crans.org)
@license: GPL3.0
"""

import time
import os

import RPi.GPIO as GPIO
import vlc

GPIO.setmode(GPIO.BCM)

# pin sur lequel est branché l'interrupteur du combiné
enable_pin_telephone_mural = 4

# joue si l'interrupteur est ouvert : 1
# joue si l'interrupteur est fermé : 0
joue_si_etat = 1


nom_fichier_telephone_mural = os.path.join("telephone_mural", "00_Sacha_Guitry.mp3")

def telephone_mural():
    time.sleep(0.1)
    son = vlc.MediaPlayer(nom_fichier_telephone_mural)
    is_playing = False

    # initialisation des boutons
    GPIO.setup(enable_pin_telephone_mural, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    while True:
        if GPIO.input(enable_pin_telephone_mural) == joue_si_etat:
            if not is_playing:
                is_playing = True
                son.play()
        else:
            if is_playing:
                son.stop()
                is_playing = False
            time.sleep(0.01)

if __name__=="__main__":
    telephone_mural()
