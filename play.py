#!/usr/bin/env python
# coding: utf-8
"""
Play

Script principal pour les manipulations TAKK/Aveam/Cité des telecoms

@author: Laurent Malys (lo@crans.org)
@license: GPL3.0
"""

__version__ = "0.9"

from telephone_orange import *
from standard import *
from strowger import *
from telephone_mural import *

manip = "telephone_orange"
#manip = "standard"
#manip = "strowger"
#manip = "telephone_mural"

if __name__ == "__main__":
    if manip == "telephone_orange":
        telephone_orange()
    elif manip == "standard":
        standard()
    elif manip == "strowger":
        strowger()
    elif manip == "telephone_mural":
        telephone_mural()
